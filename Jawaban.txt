Tugas SQL

1. buat Database
	- create database myshop;

2. Membuat Table di Dalam Database
	- create table users(
	id int NOT NULL AUTO_INCREMENT Primary Key,
	name varchar(255),
	email varchar(255),
	password varchar(255)
	) engine = InnoDB;

	- create table categories(
	id int NOT NULL AUTO_INCREMENT Primary Key,
	name varchar(255)
	) engine = InnoDB;
	- create table items(
	id int not null auto_increment primary key,
	name varchar(255),
	description varchar(255),
	price int,
	category_id int,
	foreign key(category_id) references categories (id)
	) engine = InnoDB;

3. Memasukkan Data pada Table
- Insert into users (id,name,email,password)
  values ('01,'john Doe','john@doe.com','john123'),
	('02','Jane Doe','jane@doe.com','jenita123);

- Insert into categories(id,name)
  value ('01,'gadget'),
	('02','cloth'),
	('03','men'),
	('04','women'),
	('05','branded');

- Insert into items(id,name,description,price,stock,category_id)
  values ('01,'Sumsang b50','hape keren dari merek sumsang',4000000,100,1),
	('02','Uniklooh','baju keren dari brand ternama',500000,50,2),
	('03','IMHO Watch','jam tangan anak yang jujur banget',2000000,10,1);

4. Mengambil Data dari Database
a. select id,name,email from users;
b.   - select * from items where price>=1000000;
     - select * from items where name like '%sang%;

c. select * from items inner join categories
	on items.category_id = categories.id;

5. update items set price = 2500000 where id="01";